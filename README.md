## vue-json-schema-form 活动编辑器
当前项目实际为 [vue-json-schema-form demo项目](https://github.com/lljj-x/vue-json-schema-form/tree/master/packages/demo/demo-v2) 中活动编辑器对接UCToo开源项目管理后台的独立项目。  
开源项目地址[https://gitee.com/UCT/uctoo-vue-editor](https://gitee.com/UCT/uctoo-vue-editor)增加了本地帐号登录、UCToo应用市场帐号登录、扩展了多页面可视化配置等功能。  
提出全栈模型同构的理念，并采用[vuex-orm](https://github.com/vuex-orm/vuex-orm)、[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios) 技术选型进行了项目实现，完成了UCToo技术体系的SaaS共享数据模型规划。  
全栈模型同构可以很好的降低前后端数据状态同步的复杂度以及简化复杂业务逻辑的算法，使得前后端算法基本保持一致，为低代码、无代码、可视化代码生成等开发需求提供了更加规范化的架构风格和高效率的开发基础设施。  

![](https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/vueeditor.png)

## 使用
UCToo线上运营版本地址 https://vueeditor.uctoo.com ，帐号与 www.uctoo.com 帐号相同。  
简易使用方案：可在uctoo.com可视化搭建页面，保存页面的Json配置数据，可在已有项目中通过API接口（nanoid传参）获取页面的Json配置数据，灵活根据Json配置数据渲染页面。  
完整使用方案：可在uctoo.com可视化搭建页面，保存页面的Json配置数据，可在已有项目中集成vue-json-schema-form渲染引擎（可参考本项目及uctoo-uni-starter项目），可自动根据Json配置数据渲染页面。与预览效果相同。  

### 启动方法  
配置.env.development、.env.production和settings.js中的后台端口地址。如有www.uctoo.com 帐号，配置为https://serv.uctoo.com 可通过uctoo帐号登录。
```
# 安装，请使用 yarn
yarn

# 开发环境
yarn dev

# 浏览器访问
http://127.0.0.1:8800/vue-editor.html

# build
yarn build

```

### 后台启动方法  
后台功能包含在UCToo develop模块中，可独立安装部署UCToo开源项目[UCToo](https://gitee.com/uctoo/uctoo)已包含develop模块。
```
// 安装 composer 扩展
composer install --ignore-platform-reqs

// 安装后台, 按照提示输入对应信息即可
php think catch:install

// 启动后台（开发环境后台端口默认地址为http://127.0.0.1:8000）
php think run

```
生产环境请使用nginx等部署后端服务，或uctoo一键云原生部署等。

### 备注
> 为了和  [vue-json-schema-form demo项目](https://github.com/lljj-x/vue-json-schema-form/tree/master/packages/demo/demo-v2) 项目保持通用性。对依赖的 `demo-common` 做了以下处理，js中使用webpack 配置别名，css 使用postcss import插件配置path来兼容。

详细如下：
* webpack 配置 vue-config.js 添加别名，解决js中 import 'demo-common/xx'
```js
config.resolve.alias = {
    ...config.resolve.alias,
    'demo-common': path.resolve(__dirname, './src/demo-common'),
};
```

* postcss.config.js 配置post import plugin 添加path配置，解决css中 @import 'demo-common/xx'
```js
require('postcss-import')({
    path: ['src/']
})
```

### 构建
使用 vue cli 构建项目多entry形式，按文件目录自动查找entry，有兴趣可参见 `/scripts/entry.js` 文件

### 为何独立一份
[vue-json-schema-form](https://github.com/lljj-x/vue-json-schema-form) 为Monorepo，package 之间有相互依赖，由于会有新人不知如何抽出需要的独立package，
所以现在独立为一个单独的仓库，同时方便后续的更新。

## 产品架构  
1. 部分代码迁移自catchadmin前端项目[catch-admin-vue](https://github.com/JaguarJack/catch-admin-vue)，覆盖到获取用户登录token之后的功能。  
2. 其余功能通过采用[vuex-orm](https://github.com/vuex-orm/vuex-orm)、[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios)等新一代技术选型进行实现。（请参考相关文档）
开发者可比对不同编程模型在代码组织、算法实现、开发理念等方面的差异。
3. models中的前端模型与UCToo后台对应模型同构，接口对应一致。有利于后续可视化、自动化代码生成。也有利于前后端合作或全栈开发思路/算法一致。
4. vue_editor_pages表保存页面信息，vue_editor_items_config表保存页面控件配置信息。
5. 主要思路是在项目初始化时从后端接口获取数据，存入前端数据库，前端可视化配置等数据状态变化都在前端数据库操作，需要持久化保存至后端时，通过接口将前端数据库同步至后端。
6. 通过关联模型可以部分实现类似graphql的效果。

### Known Bugs  
1. 新增页面后，需要手动刷新一遍页面，否则直接切换到新页面进行配置，新页面itemsConfig的page_id参数初始化错误，导致保存至数据库会出错。
2. 多次切换页面，会导致页面必须包含的全部商品item自动累计增加多次。
3. 登录本地帐号时，首次数据加载貌似有点问题，登录后手动刷新页面可正常加载。  
4. 模态对话框遮罩层bug。  

### Road Map  
1. 实现EditorM.vue页面的功能，与Editor.vue基本一致。
2. 组件的后台可视化管理，即schema、uiSchema、formData、errorSchema等的可视化管理。  
3. 图片、文件等素材的CMS管理。  
4. 组件、页面配置等数据的共享机制。  
5. 目前仅对data.js中的接口进行了模型适配，如vue-json-schema-form项目采用vuex-orm重构，可有效提高对接后台效率和简化算法。
6. playground、schema生成器与UCToo产品对接。
7. 适配UniApp组件库，添加至views/editor/viewComponentsU目录，以支持uniapp组件的可视化配置。
8. 创建UniApp移动端starter项目，添加vue-json-schema-form项目及data.js等渲染引擎到starter项目，以实现从可视化配置的json数据，移动端运行时还原渲染界面（与editor的预览功能相同）。

### 参考资料
[vue-json-schema-form](https://github.com/lljj-x/vue-json-schema-form)  
[uctoo](https://gitee.com/uctoo/uctoo)  
[catchadmin](https://catchadmin.com/)   
[vuex-orm](https://github.com/vuex-orm/vuex-orm)  
[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios)  

---

## 企业版产品定价
#### 企业版销售收入的20%捐赠开源项目

|            | 免费版 |   标准版 |     尊享版      |    私有化部署     |
| :--------: | :--: | :----------: | :-------------: | :---------: |
| 价格   |  ￥0  |   ￥50/年 | ￥600/年 | 咨询报价 |
| 页面数量   |  20  |   不限 | 不限 | 不限 |
| 图片库     |  100  |   不限 | 不限  | 不限 |
| 前端源码 |  [x] (参考本开源项目示例)  | [✓]  | [✓]   | [✓]  |
| 后端源码 |  [x] (参考UCToo开源项目示例) | [x]  | [✓]   | [✓]  |
| 独立域名 |  [x]  | [✓]  | [✓]   | [✓]  |
| 数据库 | UCToo.com SaaS 数据库 | UCToo.com SaaS 数据库  | 云原生独立部署   | 私有化部署  |
| 发布付费组件/模板 |  [x]  | [✓]  | [✓]   | [✓]  |
| 提供正规发票与合同 |  [x]  | [✓]  | [✓]   | [✓]  |
| 专属技术团队支持 |  [x]  | [✓]  | [✓]   | [✓]  |

#### 产品销售方式

1.  免费版可在uctoo.com注册帐号使用，标准版及以上提供前端源码，尊享版及以上提供后端源码。  
2.  标准版及尊享版可从 https://appstore.uctoo.com 直接购买。  
3.  大中型客户独立部署运营平台或与已有产品/项目集成方案定制开发，请联系销售代表交流需求。    
![销售代表](https://www.uctoo.com/img/team/patrick.png)  
