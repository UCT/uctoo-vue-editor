import axios from 'axios';
import authHeader from './auth-header';
import defaultSettings from '../settings';

const API_URL = defaultSettings.baseURL;

class UserService {
  getPublicContent() {
    return axios.get(API_URL + '/users', { headers: authHeader() });
  }

  getUserBoard() {
    return axios.get(API_URL + '/user/info', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + '/mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + '/admin', { headers: authHeader() });
  }
}

export default new UserService();
