import axios from 'axios';
import defaultSettings from '../settings';

const API_URL = defaultSettings.baseURL;

class AuthService {
  login(user) {
    return axios
      .post(API_URL + '/login', {
        email: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.data.token) {
          localStorage.setItem('user', JSON.stringify(response.data.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();
