import {getToken} from '../utils/auth';

export default function authHeader() {
   //  let user = JSON.parse(localStorage.getItem('user'));

  if (getToken()) {
    return { Authorization: 'Bearer ' + getToken() }; // for Spring Boot back-end
    // return { 'x-access-token': user.accessToken };       // for Node.js Express back-end
  } else {
    return {};
  }
}
