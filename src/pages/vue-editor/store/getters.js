const getters = {
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  errorLogs: state => state.errorLog.logs,
  email: state => state.user.email,
  user_id: state => state.user.id,
  user_app: state => state.user.app,
  wechatAccount: state => state.user.wechatAccount,
  appid: state => state.user.appid,
  storeinfo: state => state.user.storeinfo
};
export default getters;
