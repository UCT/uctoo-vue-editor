/**
 * Created by UCToo on 2022/11/31 15:06.
 */

import genImgItem from '../_commonConfig/error/genImgItem';

const errorItemSchema = genImgItem();

export default {
    imgItem1_1: errorItemSchema,
    imgItem1_2: errorItemSchema,
    imgItem1_3: errorItemSchema,
    imgItem2_1: errorItemSchema,
    imgItem2_2: errorItemSchema,
    imgItem2_3: errorItemSchema,
    imgItem3_1: errorItemSchema,
    imgItem3_2: errorItemSchema,
    imgItem3_3: errorItemSchema
};
