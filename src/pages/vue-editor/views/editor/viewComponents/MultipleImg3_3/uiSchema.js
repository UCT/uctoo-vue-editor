/**
 * Created by Liu.Jun on 2020/4/30 17:13.
 */

import genImgItem from '../_commonConfig/ui/genImgItem';

const lineItem = genImgItem({
    width: 100,
    height: 100,
});

export default {
    imgItem1_1: {
        'ui:title': '图片1',
        ...lineItem
    },
    imgItem1_2: {
        'ui:title': '图片2',
        ...lineItem
    },
    imgItem1_3: {
        'ui:title': '图片3',
        ...lineItem
    },
    imgItem2_1: {
        'ui:title': '图片4',
        ...lineItem
    },
    imgItem2_2: {
        'ui:title': '图片5',
        ...lineItem
    },
    imgItem2_3: {
        'ui:title': '图片6',
        ...lineItem
    },
    imgItem3_1: {
        'ui:title': '图片7',
        ...lineItem
    },
    imgItem3_2: {
        'ui:title': '图片8',
        ...lineItem
    },
    imgItem3_3: {
        'ui:title': '图片9',
        ...lineItem
    }
};
