import Cookies from 'js-cookie';

const TokenKey = 'Admin-Token';

export function getToken() {
    return Cookies.get(TokenKey);
}

export function setToken(token) {
    return Cookies.set(TokenKey, token);
}

export function removeToken() {
    return Cookies.remove(TokenKey);
}

const LocalEmailKey = 'Local-Email';

export function getLocalEmail() {
    return Cookies.get(LocalEmailKey);
}

export function setLocalEmail(email) {
    return Cookies.set(LocalEmailKey, email);
}

export function removeLocalEmail() {
    return Cookies.remove(LocalEmailKey);
}

const StoreKey = 'Store-Info';

export function getStoreInfo() {
    return Cookies.get(StoreKey);
}

export function setStoreInfo(token) {
    return Cookies.set(StoreKey, token);
}

export function removeStoreInfo() {
    return Cookies.remove(StoreKey);
}

const AppKey = 'Admin-App';

export function getApp() {
    return Cookies.get(AppKey);
}

export function setApp(app) {
    return Cookies.set(AppKey, app);
}

export function removeApp() {
    return Cookies.remove(AppKey);
}

const WechatAccount = 'WechatAccount';

export function getWechatAccount() {
    return Cookies.get(WechatAccount);
}

export function setWechatAccount(account) {
    return Cookies.set(WechatAccount, account);
}

export function removeWechatAccount() {
    return Cookies.remove(WechatAccount);
}

const AppId = 'AppId';

export function getAppId() {
    return Cookies.get(AppId);
}

export function setAppId(appid) {
    return Cookies.set(AppId, appid);
}

export function removeAppId() {
    return Cookies.remove(AppId);
}
