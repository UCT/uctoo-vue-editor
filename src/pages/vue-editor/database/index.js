import { Database } from '@vuex-orm/core';
import Users from '../models/Users';
import Permissions from '../models/Permissions';
import ItemsConfig from '../models/ItemsConfig';
import VueEditorPages from '../models/VueEditorPages';

const database = new Database();

database.register(Users);
database.register(Permissions);
database.register(ItemsConfig);
database.register(VueEditorPages);

export default database;
