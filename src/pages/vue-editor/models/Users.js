import { Model } from '@vuex-orm/core';
import {getToken} from '../utils/auth';
import Permissions from './Permissions';

export default class Users extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'users'

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            username: this.string(''),
            password: this.string(''),
            email: this.string(''),
            avatar: this.string(''),
            remember_token: this.string(''),
            creator_id: this.attr(''),
            department_id: this.attr(''),
            status: this.attr(''),
            last_login_ip: this.attr(''),
            last_login_time: this.attr(''),
            created_at: this.attr(''),
            updated_at: this.attr(''),
            deleted_at: this.attr(''),
            permissions:this.hasMany(Permissions,'user_id')
        };
    }

    static fetchById(id) {
        return this.api().get(`/users/${id}`);
    }

    //store初始化时不一定完成了登录（未能默认在header中添加token），因此此接口显式增加token获取用户信息
    static fetchByToken() {
        return this.api().get(`/user/info`,{headers: { Authorization: 'Bearer ' + getToken()},persistBy: 'create' });
    }
}
