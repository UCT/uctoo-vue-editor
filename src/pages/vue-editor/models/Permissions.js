import { Model } from '@vuex-orm/core';

export default class Permissions extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'permissions'

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            user_id:this.number(''),
            title: this.string(''),
            parent_id: this.number(''),
            route: this.string(''),
            icon: this.string(''),
            component: this.string(''),
            redirect: this.string(''),
            module: this.string(''),
            keepAlive: this.number(''),
            type: this.number(''),
            permission_mark: this.string(''),
            hidden: this.number(''),
        };
    }
}
