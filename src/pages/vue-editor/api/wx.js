import request from "../utils/request";

export function getWxapp(username) {
  return request({
    url: "/applet",
    method: "get",
    params: { creator: username }
  });
}
export function bindApplication(params) {
  return request({
    url: "/applet",
    method: "post",
    data: params
  });
}
// ↓ 微信扫码获取用户账户列表 ↓
export function qrcodeWxUsers(code) {
  return request({
    url: "/wechatlogin/wechatoauth",
    method: "get",
    params: { code }
  });
}
// ↓ 微信扫码登录后选择帐号登录 ↓
export function qrcodeWxLogin(data) {
  return request({
    url: "/wechatlogin/wechatlogin",
    method: "post",
    data
  });
}
// ↓ 微信扫码登录后选择帐号注册 ↓
export function qrcodeWxRegist(data) {
  return request({
    url: "/wechatlogin/wechatregist",
    method: "post",
    data
  });
}
// 短信验证码（阿里云通道）
export function sendSms(data) {
  return request({
    url: "/api/Dysmsapi/SendSms",
    method: "post",
    data
  });
}
/**
 * @methods
 * @description 检验短信验证码
 * @param {Object} data 请求参数
 * @param {Number} data.captcha 收到的短信验证码
 * @param {Number} data.mobile 电话号
 * @param {String} data.event 验证码用途类型
 * @returns {Promise<{code:Number,message:String,data?:Array<String>}>} 标准Axios返回的Promise
 */
export function validSmsCode(data) {
  return request({
    url: "/smsLog/smscheck",
    method: "post",
    data
  });
}

export function uploadImage(formDatas) {
  return request({
    url: "/upload/image",
    method: "post",
    headers: {
      "Content-Type": "multipart/form-data"
    },
    data: formDatas
  });
}

export function brandGet(appid) {
  return request({
    url: "/api/wechatopen/product/brand/get",
    method: "post",
    params: appid
  });
}
